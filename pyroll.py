import random
import sys
import regex as re


def roll(roll_string):
    """Calculates and returns the result of a dice roll."""
    tokens = tokenize_roll(roll_string)
    dice_total, roll_values = roll_dice(int(tokens[0]), int(tokens[1]))
    dice_min, dice_max = min_max_values(tokens, modifier=False)
    modifier_total = apply_modifier(dice_total, tokens[2], tokens[3])
    total_min, total_max = min_max_values(tokens)
    return {
        "total": modifier_total,
        "total_min": total_min,
        "total_max": total_max,
        "dice_total": dice_total,
        "roll_values": roll_values,
        "dice_min": dice_min,
        "dice_max": dice_max,
    }


def tokenize_roll(roll_string, regex="(\d*)d(\d*)([\+\-]*)(\d*)"):
    """Turns the roll string into a 4-piece tuple."""
    match = re.search(regex, roll_string)
    try:
        match_groups = match.groups()
    except AttributeError:
        print(f"Invalid input: {roll_string}")
        raise
    return (
        clean_token(match_groups[0], blank_value=1),
        clean_token(match_groups[1]),
        clean_token(match_groups[2], blank_value="+"),
        clean_token(match_groups[3]),
    )


def clean_token(token, blank_value=0):
    """Turns strings into ints and blanks into zeros."""
    if token == "":
        return blank_value
    elif token in ["-", "+"]:
        return token
    else:
        return int(token)


def roll_dice(number_of_dice, dice_sides):
    """Rolls and returns the sum of a single or multiple dice rolls."""
    total = 0
    dice_rolls = []
    while number_of_dice > 0:
        roll_value = roll_die(dice_sides)
        dice_rolls.append(roll_value)
        total += roll_value
        number_of_dice -= 1
    return total, dice_rolls


def roll_die(sides):
    """Returns the result of a single die roll."""
    return random.choice(range(1, sides + 1))


def apply_modifier(dice_total, op, modifier_value):
    """Adds or subtracts the modifier value from the dice total."""
    if op == "+":
        return dice_total + modifier_value
    elif op == "-":
        return dice_total - modifier_value
    else:
        print(f"Error. Bad operation value: {op}")
        raise ValueError


def min_max_values(tokens, modifier=True):
    """For fun, return the potential min and max for roll."""
    min_dice = tokens[0]
    max_dice = tokens[0] * tokens[1]
    if modifier:
        min_total = apply_modifier(min_dice, tokens[2], tokens[3])
        max_total = apply_modifier(max_dice, tokens[2], tokens[3])
        return min_total, max_total
    else:
        return min_dice, max_dice


def print_roll_results(roll):
    """Prints the results of a roll."""
    print(
        f"Dice Rolls: {roll['roll_values']} = {roll['dice_total']} (Dice min/max: {roll['dice_min']}/{roll['dice_max']})"
    )
    print(f"Total: {roll['total']} (Min/Max : {roll['total_min']}/{roll['total_max']})")


def main():
    print(sys.argv[1])
    print_roll_results(roll(sys.argv[1]))


if __name__ == "__main__":
    main()
